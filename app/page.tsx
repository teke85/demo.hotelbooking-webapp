import { Button } from '@/components/ui/button';

export default function Home() {
  return (
    <main className="flex flex-col font-semibold min-h-screen p-5">
      <div className="flex flex-row items-center justify-between">
        Hello Auth
        
      </div>
      <Button variant="secondary" className="w-20">
        Click me
      </Button>
    </main>
  );
}
