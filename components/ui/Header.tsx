import { SignedIn, SignedOut, SignInButton, UserButton } from '@clerk/nextjs';
import Link from 'next/link';
import { Button } from './button';
import { ModeToggle } from './mode-toggle';

const Header = () => {
  return (
    <header>
      <nav className="container flex items-center justify-between">
        <ul className="flex gap-6">
          <li>
            <Link href="/">Home</Link>
          </li>
        </ul>
        <div className="flex gap-6">
          <ModeToggle />
          <SignedOut>
            <SignInButton>
              <Button>Sign In</Button>
            </SignInButton>
          </SignedOut>
          <SignedIn>
            <UserButton />
          </SignedIn>
        </div>
      </nav>
    </header>
  );
};

export default Header;
